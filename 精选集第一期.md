Flutter 自 2015 年推出以来，凭借着其极高的开发交付效率，优秀的多平台能力，以及强大的 UI 表现力，受到了许多开发者们的推崇。虽然 Flutter 的确仍旧存在一些问题，但依然是不少开发者们在跨平台开发时的第一选择。

今天 Gitee 为开发者们推荐的就是几款优秀的 Flutter 开源项目，希望不论是接触 Flutter 的新人，还是使用 Flutter 很久的老炮，都会有所收获。（点击图片即可前往项目主页）

![输入图片说明](https://images.gitee.com/uploads/images/2020/1123/115454_0c0d05aa_8189591.jpeg "1.jpg")

跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
1.一个仿瑞幸咖啡的 Flutter 应用，完成度很高。项目内还提供了原型、设计图和 API，十分方便。
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
2.Flutter 视频/直播 APP 省流量&加速解决方案。几行代码即可在现有 Flutter 项目中快速集成，通过预加载形式实现 P2P 加速，完全不影响用户的播放体验。
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
3.基于 Flutter 开发的 2048 游戏。支持 3×3、4×4、6×6 三种模式，对于 Flutter 初学者来说是一个很好的学习项目。
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
4.MXFlutter 是一套基于 JavaScript 的 Flutter 框架。可以用极其类似 Dart 的开发方式，通过编写 JavaScript 代码，来开发 Flutter 应用。
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
5.Flutter 内置的 Material Design 风格图标在实际使用中是远远不够的，这个项目就是让开发者们在 Flutter 中引入可自定义的图标。
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
6.一个 Flutter 富文本编辑器项目。支持图文和视频的混合排版。
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
跨平台开发首选，这些 Flutter 项目你应该了解-Gitee 官方博客
关于每周项目精选
Gitee 每周项目精选，为你推荐 Gitee 上有趣又好玩的项目，也邀请大家推荐更多项目，帮助好的项目获得曝光！

欢迎开发者们推荐项目，每个月会抽取幸运推荐人，赠送 Gitee 鼠标垫，文化衫，马克杯等小礼品。

现在就点击后方链接，前往仓库提交 Issue，推荐你心中那些好玩又有趣的开源项目吧：https://gitee.com/Selected-Activities/weekly-collection